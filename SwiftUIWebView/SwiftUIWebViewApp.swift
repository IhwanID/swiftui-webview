//
//  SwiftUIWebViewApp.swift
//  SwiftUIWebView
//
//  Created by Ihwan ID on 13/12/20.
//

import SwiftUI

@main
struct SwiftUIWebViewApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
