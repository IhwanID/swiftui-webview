//
//  ContentView.swift
//  SwiftUIWebView
//
//  Created by Ihwan ID on 13/12/20.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView{
            SwiftUIWebView(url: URL(string: "https://ihwan.id"))
                
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
